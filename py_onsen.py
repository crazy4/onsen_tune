# -*- coding: utf-8 -*-

import requests
import datetime
import md5
from xml.etree.ElementTree import *

#get the program info
def get_prog_xml(wday):
    now = datetime.datetime.now()
    code = md5.new("onsen" + str(now.weekday() + 1) + str(now.day) + str(now.hour)).hexdigest()
    postdata = "file_name=regular_" + str(wday) + "&code=" + code
    query = { "code" : code, "file_name" : "regular_" + str(wday) }
    r = requests.post("http://onsen.ag/getXML.php", query)
    return r.content


prog = [0] * 5
yobi = [u"月曜日", u"火曜日", u"水曜日", u"木曜日", u"金曜日"]

for wday in range(5):
    prog[wday] = get_prog_xml(wday + 1)

# XML parsing
for i in range(5):
    print "----------------------------------------------------------------"
    print yobi[i] + "\n"

    elem = fromstring(prog[i])
    for item in elem.getiterator("program"):
        print item.findtext("title")
        for it in item.getiterator("contents"):
            print it.findtext("fileUrl")
        print "\n"
